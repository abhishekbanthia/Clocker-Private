// Copyright © 2015 Abhishek Banthia

import Cocoa

class Keys {
    
    static var CLDecodingKey: String = "Enter Google API Key here."
    
    class func applicationDidLaunch() {
        var plistFormat =  PropertyListSerialization.PropertyListFormat.xml
        
        guard let plistPath = Bundle.main.path(forResource: "Privates", ofType: "plist"),
            let plistXML = FileManager.default.contents(atPath: plistPath) else {
            assertionFailure()
            return
        }
        
        do {
            let plistData = try PropertyListSerialization.propertyList(from: plistXML,
                                                                       options: .mutableContainersAndLeaves,
                                                                       format: &plistFormat)
            if let plistDictionary = plistData as? [String: Any],
                let presentKey = plistDictionary["DecodingKey"] as? String {
                CLDecodingKey = presentKey
            }
        } catch {
            print("Error reading plist: \(error), format: \(plistFormat)")
        }
    }

}
